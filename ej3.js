function ordena(objeto, orden){ // Función que se encarga de ordenar los objetos
    if (orden=="categoria") {
        objeto.sort(function (a, b){
            return (a.cat - b.cat)
        })
    }
    return objeto;
}
function pintaTienda(){
    var tienda=localStorage.getItem("tienda");
    if(tienda!=null){
        tienda=JSON.parse(tienda); // Parseo a --> objeto
        tienda=ordena(tienda, "categoria")
        var cadena="<tr><td>id</td><td>cat</td><td>nombre</td><td>unidades</td><td>precio</td><td></td><td></td></tr>";
        for(i=0;i<tienda.length;i++){
            cadena+="<tr>";
            cadena+="<td>"+tienda[i].id+"</td><td>"+tienda[i].cat+"</td><td>"+tienda[i].nombre+"</td><td>"+tienda[i].unidades+
            "</td><td>"+tienda[i].precio+"</td><td><input type='text'id='textAdd_"+tienda[i].id+"' name='qty'></td><td><input type='button' class='btAdd' id='btAdd_"+tienda[i].id+"' value='Añadir'></td>";
            cadena+="</tr>";
        }   
        $("#capaSalidaTienda").html("<table>"+cadena+"</table>");
    }
}
function procesaRespuesta(tienda){
    localStorage.setItem('tienda',tienda); // Guardo en localStotage
    pintaTienda(tienda);
}

function obtenerDatos()
{
    var url = "http://hispabyte.net/DWEC/entregable2-3.php";
    $.ajax({
        url: url,
        method: 'GET',
    }).done(function(tienda) {
        console.log(tienda);
        procesaRespuesta(tienda);
    }).fail(function(err) {
        throw err;
    });
}

function addCarrito(idArticulo, orderQty){        
    var carrito=[];
    if(localStorage.getItem("carrito")!= null){
        carrito=JSON.parse(localStorage.getItem("carrito"));    
    }
    var tienda=JSON.parse(localStorage.getItem("tienda"));
    for(i=0;i<tienda.length;i++){
        var aux="";
        if(tienda[i].id==idArticulo){
            aux=tienda[i];
            aux.unidades=orderQty;
            carrito.push(aux);
        }
    }
    carrito=JSON.stringify(carrito); 
    localStorage.setItem('carrito',carrito); // Guardo en localStotage
}

function pintaCarrito(){
    var carrito=JSON.parse(localStorage.getItem("carrito"));
    var cadena="<tr><td>id</td><td>cat</td><td>nombre</td><td>unidades</td><td>precio</td><td></td><td></td></tr>";
    if (carrito!= null) {
        for(i=0;i<carrito.length;i++){
            cadena+="<tr>";
            cadena+="<td>"+carrito[i].id+"</td><td>"+carrito[i].cat+"</td><td>"+carrito[i].nombre+"</td><td>"+carrito[i].unidades+
            "</td><td>"+carrito[i].precio+"</td>";
            cadena+="</tr>";
        }
        $("#capaSalidaCarrito").html("<table>"+cadena+"</table>");
    }
}

function vaciarTienda(){
    var tienda=JSON.parse(localStorage.getItem("tienda"));
    localStorage.removeItem('tienda',tienda);
    $("#capaSalidaTienda").html("");
}
function vaciarCarrito(){
    var carrito=JSON.parse(localStorage.getItem("carrito"));
    localStorage.removeItem('carrito',carrito);
    pintaCarrito();
    $("#capaSalidaCarrito").html("");
}

function getIdArticulo(e){
    var o=e.target.id; // id del botón
    var caracter="_";
    var pos=o.indexOf(caracter,0);
    pos++;
    var idArticulo=o.substr(pos);
    //alert('Id artículo: ' + idArticulo);
    return idArticulo;
}

function getOrderQty(idArticulo){
    idArticulo=idArticulo.toString();
    var orderQty=$('#textAdd_'+idArticulo).val();
    return orderQty;
}

function esNum(idArticulo){
    var orderQty=getOrderQty(idArticulo);
    if (isNaN(parseInt(orderQty))) {
        return false;
    }else{
        if (orderQty % 1 != 0 || orderQty<1) {
            return false;
        }else{
            return true;
        }
    }
}

function checkDisponible(idArticulo, orderQty){
    var tienda=localStorage.getItem("tienda");
    var tiendaQty=0;
    if(tienda!=null){
        tienda=JSON.parse(tienda); // Parseo a --> objeto
        for(i=0;i<tienda.length;i++){
            if(tienda[i].id==idArticulo){
                tiendaQty=tienda[i].unidades;
            }
        }
    }
    var carritoQty=0;
    var carrito=localStorage.getItem("carrito");
    if(carrito!=null){
        carrito=JSON.parse(carrito); // Parseo a --> objeto
        for(i=0;i<carrito.length;i++){
            if(carrito[i].id==idArticulo){
                carritoQty=parseInt(carritoQty)+parseInt(carrito[i].unidades);
            }
        }
    }
    var totalpedidas=parseInt(orderQty)+parseInt(carritoQty);

    if (tiendaQty<totalpedidas) {
        return false;
    }else{
        return true;
    }
}

// Al carga la pagina asociamos al onclick del boton el método
$(document).ready(function(){
    if (localStorage.getItem("tienda")!=null) {
        pintaTienda();
    }else{        
        obtenerDatos();
        pintaTienda();
    }
    if (localStorage.getItem("carrito")!=null) {

        pintaCarrito();
    }
    $("#recargaTienda").click(function(e){
        obtenerDatos();
        vaciarCarrito();
    });
    $("#vaciaTienda").click(function(e){
        vaciarTienda();
        vaciarCarrito();
    });
    $("#vaciaCarrito").click(vaciarCarrito);
    
    // Añade artículo
    $(this).click(function(e){
        var clase=$(e.target).attr('class');
        if (clase=='btAdd') {
            if (esNum(getIdArticulo(e))==true) {
                if (checkDisponible(getIdArticulo(e), getOrderQty(getIdArticulo(e)))==true) {
                    addCarrito(getIdArticulo(e), getOrderQty(getIdArticulo(e)));
                    pintaCarrito();
                }else{
                    alert("No hay stock suficiente");
                }
            }else{
                alert("Debes introducir un número entero mayor o igual a 1")
            }
        }
    });
})
